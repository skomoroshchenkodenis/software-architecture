#pragma once

#include "scope.h"

class RootScope : public IScope {
public:
    RootScope() {
        ResolveDependencyStrategy registerStrategy = [this](std::vector<std::any> params) -> std::shared_ptr<ICommand> {
            return std::make_shared<RegisterCommand>(std::any_cast<std::string>(params.at(0)),
            std::any_cast<ResolveDependencyStrategy>(params.at(1)),
            std::shared_ptr<IScope>(this, [](void*){
                return;
           }));
        };
        set("Ioc.Register",registerStrategy);
    }

    void set(const std::string& name, ResolveDependencyStrategy strategy) override {
        m_Resolvers[name] = std::move(strategy);
    }

    ResolveDependencyStrategy get(const std::string& name) override {
        if(!m_Resolvers.contains(name)) {
            throw std::invalid_argument("Unregistered resolve name");
        }
        return m_Resolvers[name];
    }

    std::map<std::string, ResolveDependencyStrategy> m_Resolvers;
};