#pragma once

#include <string>
#include <map>
#include <vector>
#include <any>
#include <functional>

using ResolveDependencyStrategy = std::function<std::any(std::vector<std::any>)>;

class IScope {
public:
    virtual ~IScope() = default;

    virtual void set(const std::string& name, ResolveDependencyStrategy strategy) = 0;
    virtual ResolveDependencyStrategy get(const std::string& name) = 0;
};