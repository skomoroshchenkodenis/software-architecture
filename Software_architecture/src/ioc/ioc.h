#pragma once

#include <string>
#include <map>
#include <functional>
#include <any>
#include <exception>
#include <memory>
#include <stdexcept>

#include "../commands/registercommand.h"
#include "rootscope.h"
#include "../exceptions/iocscopeexception.h"

ResolveDependencyStrategy lambdaConverter(auto&& lambda) {
    return ResolveDependencyStrategy(lambda);
}

class Ioc {


public:

    static void init() {
        setScope(std::make_shared<RootScope>());
    }

    static void clear() {
        setScope(nullptr);
    }

    template<typename T, typename... Args>
    static std::shared_ptr<T> resolve(const std::string& name, Args&&... args) {
        if(!m_CurrentScope) {
            throw IocScopeException("No active scope");
        }
        std::vector<std::any> params = {std::forward<Args>(args)...};
        try {
            auto resolver = m_CurrentScope->get(name);
            auto result = std::any_cast<std::shared_ptr<T>>(resolver(params));
            return result;
        } catch(std::invalid_argument& ex) {
            throw ex;
        }
    }

    static void setScope(std::shared_ptr<IScope> scope) {
        m_CurrentScope = std::move(scope);
    }

    /*
    template<typename T, typename... Args>
    std::shared_ptr<T> resolve(const std::string& name, Args&&... args) {
        auto resolver = std::static_pointer_cast<DependencyResolver<T, std::remove_cvref_t<Args>...>>(m_Resolvers[name]);
        return resolver->resolve(std::forward<Args>(args)...);
    }

    template<typename T, typename... Args>
    void registerObject(const std::string& name, std::function<std::shared_ptr<T> (Args...)> functor) {
        m_Resolvers[name] = std::make_shared<DependencyResolver<T, Args...>>(std::move(functor));
    }
    */
private:
    static std::shared_ptr<IScope> m_CurrentScope;
};