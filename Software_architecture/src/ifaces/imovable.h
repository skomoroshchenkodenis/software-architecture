#pragma once

#include <utility>
#include <memory>

#include "../commands/icommand.h"
#include "utils.h"

template<typename T>
class IMovable {
public:
    virtual ~IMovable() = default;

    virtual void setPosition(const T& position) = 0;
    virtual T getPosition() const = 0;

    virtual T getVelocity() const = 0;

    virtual void setMoveCommand(const std::shared_ptr<ICommand>& command) = 0;
    virtual std::shared_ptr<ICommand> getMoveCommand() const = 0;
};
