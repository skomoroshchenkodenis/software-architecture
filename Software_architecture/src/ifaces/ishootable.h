#pragma once

#include <memory>

#include "iuobject.h"

template <typename Dim>
class IShootable {
public:
    virtual ~IShootable() = default;

    virtual std::string getProjectile() = 0;
    virtual Dim getDirection() = 0;
};