#pragma once

#include <memory>

#include "../commands/icommand.h"

template<typename T>
class IQueue {
public:
    virtual ~IQueue() = default;

    virtual void push(std::shared_ptr<T> command) = 0;
    virtual std::shared_ptr<T> pop() = 0;
};