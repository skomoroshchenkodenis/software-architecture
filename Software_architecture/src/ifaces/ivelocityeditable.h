#pragma once

#include "utils.h"

template<typename T>
class IVelocityEditable {
public:
    virtual ~IVelocityEditable() = default;

    virtual void setVelocity(const T& position) = 0;
};
