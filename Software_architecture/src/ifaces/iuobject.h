#pragma once

#include <any>
#include <string>

class IUObject {
public:
    virtual ~IUObject() {}

    virtual std::any getProperty(const std::string& name) = 0;
    virtual void setProperty(const std::string& name, const std::any& value) = 0;
};
