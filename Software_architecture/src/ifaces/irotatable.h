#pragma once

#include "utils.h"
#include "../commands/icommand.h"

template<typename T>
class IRotatable {
public:
    virtual ~IRotatable() = default;

    virtual double getAngularVelocity() const = 0;

    virtual T getDirection() const = 0;
    virtual void setDirection(const T& direction) = 0;

    virtual void setRotateCommand(const std::shared_ptr<ICommand>& command) = 0;
    virtual std::shared_ptr<ICommand> getRotateCommand() const = 0;
};
