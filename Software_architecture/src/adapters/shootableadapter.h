#pragma once

#include "../ifaces/ishootable.h"
#include "../ifaces/iuobject.h"

template<typename Dim = vec2_t<double>>
class ShootableAdapter : public IShootable<Dim> {
public:
    ShootableAdapter(std::shared_ptr<IUObject> object) noexcept: m_Object(std::move(object)) { }

    std::string getProjectile() override {
        return std::any_cast<std::string>(m_Object->getProperty("projectile"));
    };

    Dim getDirection() override {
        return std::any_cast<Dim>(m_Object->getProperty("direction"));
    }

private:
    std::shared_ptr<IUObject> m_Object;
};