#pragma once

#include <memory>

#include "ifaces/ivelocityeditable.h"
#include "ifaces/iuobject.h"

template<typename T = vec2_t<double>>
class VelocityEditableAdapter : public IVelocityEditable<T> {
public:
    explicit VelocityEditableAdapter(std::shared_ptr<IUObject> object) noexcept : m_Object(std::move(object)){}

    void setVelocity(const T& velocity) override {
        m_Object->setProperty("velocity", std::any(velocity));
    }
private:
    std::shared_ptr<IUObject> m_Object;
};