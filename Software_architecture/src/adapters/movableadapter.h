#pragma once

#include "ifaces/imovable.h"
#include "ifaces/iuobject.h"

#include <memory>

template<typename T = vec2_t<double>>
class MovableAdapter : public IMovable<T> {
public:
    MovableAdapter(std::shared_ptr<IUObject> object) noexcept : m_Object(std::move(object)) {}

    virtual void setPosition(const vec2_t<double>& position) override {
        m_Object->setProperty("position", std::any(position));;
    }
    virtual T getPosition() const override {
        return std::any_cast<T>(m_Object->getProperty("position"));
    }

    virtual T getVelocity() const override {
        return std::any_cast<T>(m_Object->getProperty("velocity"));
    }

    void setMoveCommand(const std::shared_ptr<ICommand>& command) override {
        m_Object->setProperty("moveCommand", std::any(command));
    }

    std::shared_ptr<ICommand> getMoveCommand() const override {
        return std::any_cast<std::shared_ptr<ICommand>>(m_Object->getProperty("moveCommand"));
    }
private:
    std::shared_ptr<IUObject> m_Object;
};
