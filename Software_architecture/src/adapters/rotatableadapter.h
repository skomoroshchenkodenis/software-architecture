#pragma once

#include <memory>

#include "ifaces/irotatable.h"
#include "ifaces/iuobject.h"

template<typename T = vec2_t<double>>
class RotatableAdapter : public IRotatable<T> {
public:
    explicit RotatableAdapter(std::shared_ptr<IUObject> object)  : m_Object(std::move(object)) {}

    double getAngularVelocity() const override {
        return std::any_cast<double>(m_Object->getProperty("angularVelocity"));
    }

    T getDirection() const override {
        return std::any_cast<T>(m_Object->getProperty("direction"));
    }

    void setDirection(const T& direction) override {
        m_Object->setProperty("direction", std::any(direction));
    }

    void setRotateCommand(const std::shared_ptr<ICommand>& command) override {
        m_Object->setProperty("rotateCommand", std::any(command));
    }

    std::shared_ptr<ICommand> getRotateCommand() const override {
        return std::any_cast<std::shared_ptr<ICommand>>(m_Object->getProperty("rotateCommand"));
    }
private:
    std::shared_ptr<IUObject> m_Object;
};