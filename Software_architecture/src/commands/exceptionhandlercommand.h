#pragma once

#include <memory>

#include "icommand.h"

class ExceptionHandlerCommand : public ICommand {
public:
    ExceptionHandlerCommand(std::shared_ptr<ICommand> command) : m_Command(std::move(command)) { }

    void execute() override {
        try {
           // m_Command->undo();
        } catch (...) { }
    }

private:
    std::shared_ptr<ICommand> m_Command;
};