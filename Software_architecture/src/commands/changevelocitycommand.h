#pragma once

#include <cmath>
#include <memory>

#include "../game/iobjectsstorage.h"
#include "commands/icommand.h"
#include "../ifaces/iuobject.h"
#include "../adapters/velocityeditableadapter.h"
#include "../adapters/movableadapter.h"
#include "../adapters/rotatableadapter.h"
#include "utils.h"
#include "../ioc/ioc.h"
#include "../exceptions/commandexception.h"

template<typename T>
class ChangeVelocityCommand : public ICommand {
public:
    ChangeVelocityCommand(uint64_t objectId) noexcept : m_ObjectId(objectId) {}

    void execute() override {
        try {
            std::shared_ptr<IObjectsStorage> storage;
            storage = Ioc::resolve<IObjectsStorage>("Storage");
            auto object = storage->getObject(m_ObjectId);
            if(!object) {
                return;
            }

            MovableAdapter<T> movableAdapter(object.value());
            RotatableAdapter<T> rotatableAdapter(object.value());
            VelocityEditableAdapter<T> velocityEditableAdapter(object.value());

            auto direction = rotatableAdapter.getDirection();
            auto velocity = movableAdapter.getVelocity();
            m_StartVelocity = velocity;
            double angle = math::angleBetweenVec2(velocity, direction);
            velocityEditableAdapter.setVelocity(math::rotateVec2(velocity, math::degrees(angle)));
        } catch (std::exception& ex) {
            throw CommandException("No available storages found", "PrintHandler");
        }
    }

private:
    uint64_t m_ObjectId;
    T m_StartVelocity;
};