#pragma once

#include <vector>
#include <memory>

#include "icommand.h"

class MacroCommand : public ICommand {
public:
    MacroCommand(std::vector<std::shared_ptr<ICommand>> commands = {}) noexcept : m_Commands(std::move(commands)) {

    }

    void addCommand(std::shared_ptr<ICommand> command) {
        m_Commands.push_back(std::move(command));
    }

    void execute() override {
        for(const auto& command : m_Commands) {
            command->execute();
        }
    }

private:
    std::vector<std::shared_ptr<ICommand>> m_Commands;
};