#pragma once

#include "icommand.h"

class EmptyCommand : public ICommand {
public:
    EmptyCommand() = default;

    void execute() override { }
};