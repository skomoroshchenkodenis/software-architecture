#pragma once

#include "icommand.h"
#include "emptycommand.h"
#include "../ifaces/iuobject.h"
#include "../adapters/movableadapter.h"
#include "../ioc/ioc.h"
#include "../game/iobjectsstorage.h"
#include "../exceptions/commandexception.h"

class EndMoveCommand : public ICommand {
public:
    EndMoveCommand(uint64_t objectId) noexcept : m_ObjectId(objectId) { }

    void execute() override {
        try {
            std::shared_ptr<IObjectsStorage> storage;
            storage = Ioc::resolve<IObjectsStorage>("Storage");
            auto object = storage->getObject(m_ObjectId);
            if(!object) {
                return;
            }

            MovableAdapter adapter(object.value());
            m_MoveCommand = adapter.getMoveCommand();
            adapter.setMoveCommand(std::make_shared<EmptyCommand>());
        } catch (std::exception& ex) {
            throw CommandException("No available storages found", "PrintHandler");
        }
    }

private:
    std::shared_ptr<ICommand> m_MoveCommand;
    uint64_t m_ObjectId;
};