#pragma once

#include "icommand.h"
#include "movecommand.h"

#include "../ifaces/iuobject.h"
#include "../ioc/ioc.h"
#include "../adapters/shootableadapter.h"
#include "../ifaces/iqueue.h"
#include "../exceptions/commandexception.h"

template<typename T>
class ShootCommand : public ICommand {
public:
    ShootCommand(uint64_t objectId) noexcept : m_ObjectId(objectId) {}

    void execute() override {
        try {
            std::shared_ptr<IObjectsStorage> storage;
            storage = Ioc::resolve<IObjectsStorage>("Storage");
            auto object = storage->getObject(m_ObjectId);
            if(!object) {
                return;
            }

            ShootableAdapter adapter(object.value());
            std::shared_ptr<IUObject> projectile;

            try {
                projectile = Ioc::resolve<IUObject>(adapter.getProjectile());
                std::shared_ptr<ICommand> moveCommand = std::make_shared<MoveCommand<T>>(storage->addObject(projectile));
                Ioc::resolve<IQueue<ICommand>>("Queue")->push(moveCommand);
            } catch (std::exception& ex) {
                throw CommandException("No available storages found", "PrintHandler");
            }
        } catch (std::exception& ex) {
            throw CommandException("No available storages found", "PrintHandler");
        }
    }

private:
    uint64_t m_ObjectId;
};