#pragma once

#include "icommand.h"

#include "../game/eventloop.h"

class HardResetCommand : public ICommand {
public:
    HardResetCommand(std::shared_ptr<EventLoop> loop) : m_Loop(loop){}

    void execute() override {
        m_Loop->finish();
    }

private:
    std::shared_ptr<StaticThreadPool> m_Loop;
};