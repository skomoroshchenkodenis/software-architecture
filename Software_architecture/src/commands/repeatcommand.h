#pragma once

#include "icommand.h"
#include "emptycommand.h"
#include "../ifaces/iqueue.h"

class RepeatCommand : public ICommand {
public:
    RepeatCommand(std::shared_ptr<ICommand> commandToRepeat, IQueue<ICommand>& queue) noexcept:
                    m_CommandToRepeat(commandToRepeat), m_Queue(queue) {

    }

    void execute() override {
        m_Queue.push(m_CommandToRepeat);
    }

private:
    IQueue<ICommand>& m_Queue;
    std::shared_ptr<ICommand> m_CommandToRepeat;
};