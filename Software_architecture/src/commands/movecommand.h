#pragma once

#include "commands/icommand.h"
#include "../ifaces/iuobject.h"
#include "../adapters/movableadapter.h"
#include "../ioc/ioc.h"
#include "../game/iobjectsstorage.h"
#include "../exceptions/commandexception.h"

template<typename T>
class MoveCommand : public ICommand {
public:

    MoveCommand(uint64_t objectId) noexcept : m_ObjectId(objectId) {}

    void execute() override {

        try {
            std::shared_ptr<IObjectsStorage> storage;
            storage = Ioc::resolve<IObjectsStorage>("Storage");
            auto object = storage->getObject(m_ObjectId);
            if(!object) {
                return;
            }

            MovableAdapter<T> adapter(object.value());

            auto velocity = adapter.getVelocity();
            auto position = adapter.getPosition();
            m_StartPosition = position;
            adapter.setPosition(position + velocity);
        } catch (std::exception& ex) {
            throw CommandException("No available storages found", "PrintHandler");
        }
    }

private:
    uint64_t m_ObjectId;
    T m_StartPosition;
};
