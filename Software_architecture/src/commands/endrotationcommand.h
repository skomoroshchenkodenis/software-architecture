#pragma once

#include "icommand.h"
#include "emptycommand.h"
#include "../ifaces/iuobject.h"
#include "../adapters/rotatableadapter.h"
#include "../ioc/ioc.h"
#include "../game/iobjectsstorage.h"
#include "../exceptions/commandexception.h"

#include <memory>

class EndRotationCommand : public ICommand {
public:
    EndRotationCommand(uint64_t objectId) noexcept : m_ObjectId(objectId) { }

    void execute() override {
        try {
            std::shared_ptr<IObjectsStorage> storage;
            storage = Ioc::resolve<IObjectsStorage>("Storage");
            auto object = storage->getObject(m_ObjectId);
            if(!object) {
                return;
            }

            RotatableAdapter adapter(object.value());
            m_RotateCommand = adapter.getRotateCommand();
            adapter.setRotateCommand(std::make_shared<EmptyCommand>());
        } catch (std::exception& ex) {
            throw CommandException("No available storages found", "PrintHandler");
        }
    }

private:
    std::shared_ptr<ICommand> m_RotateCommand;
    uint64_t m_ObjectId;
};