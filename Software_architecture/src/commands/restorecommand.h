#pragma once

#include <vector>

#include "icommand.h"
#include "../ifaces/iuobject.h"
#include "../ioc/ioc.h"
#include "../game/iobjectsstorage.h"

class RestoreCommand : public ICommand {
public:
    RestoreCommand(uint64_t id) : m_Id(id) {}

    void execute() override {
        try {
            auto storage = Ioc::resolve<IObjectsStorage>("Storage");
            auto object = storage->getObject(m_Id);
            if(!object) {
                return;
            }
            auto savedFields = std::any_cast<std::map<std::string, std::any>>(object.value()->getProperty("CurrentState"));
            for(const auto& [field, value]: savedFields) {
                object.value()->setProperty(field, value);
            }
        } catch (std::runtime_error& ex) {
            throw ex;
        }
    }

private:
    uint64_t m_Id;
};