#pragma once

#include "icommand.h"

#include <string>
#include <functional>
#include <initializer_list>
#include <iostream>
#include "../ioc/scope.h"

class RegisterCommand : public ICommand {
public:
    RegisterCommand(std::string name, ResolveDependencyStrategy strategy, std::shared_ptr<IScope> scope) :
    m_RegisterName(std::move(name)),
    m_Strategy(std::move(strategy)), m_Scope(std::move(scope)) { }

    void execute() override {
        m_Scope->set(m_RegisterName, m_Strategy);
    }

private:
    std::string m_RegisterName;
    std::function<std::any(std::vector<std::any>)> m_Strategy;
    std::shared_ptr<IScope> m_Scope;
};