#pragma once

#include <stdexcept>
#include <iostream>

#include "../icommand.h"

class DefaultExceptionHandler : public ICommand {
public:
    DefaultExceptionHandler(std::runtime_error ex) : m_Exception(std::move(ex)) {}

    void execute() override {
        std::cerr << m_Exception.what();
    }
private:
    std::runtime_error m_Exception;
};