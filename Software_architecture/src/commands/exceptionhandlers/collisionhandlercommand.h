#pragma once

#include "commands/icommand.h"
#include "exceptions/collisionexception.h"
#include "ioc/ioc.h"
#include "game/iobjectsstorage.h"

class CollisionHandlerCommand : public ICommand {
public:
    CollisionHandlerCommand(std::runtime_error& exception) : m_Exception(std::move(dynamic_cast<CollisionException&>(exception))){ }

    void execute() override {
        try {
            auto storage = Ioc::resolve<IObjectsStorage>("Storage");
            for(auto& id : m_Exception.getObjects()) {
                storage->deleteObject(id);
            }
        } catch (...) {}

    }

private:
    CollisionException m_Exception;
};