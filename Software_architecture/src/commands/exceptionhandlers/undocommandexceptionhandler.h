#pragma once

#include <stdexcept>

#include "../icommand.h"
#include "../restorecommand.h"
#include "../../exceptions/commandobjectexception.h"

class UndoExceptionCommandHandler : public ICommand {
public:
    UndoExceptionCommandHandler(std::runtime_error& ex) : m_Exception(dynamic_cast<CommandObjectException&>(ex)) { }

    void execute() override {
        RestoreCommand command(m_Exception.getId());
        try {
            command.execute();
        } catch(...) {}

    }

private:
    CommandObjectException m_Exception;
};