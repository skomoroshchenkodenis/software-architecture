#pragma once

#include "icommand.h"
#include "../ifaces/iuobject.h"
#include "../adapters/rotatableadapter.h"
#include "../ioc/ioc.h"
#include "utils.h"
#include "../game/iobjectsstorage.h"
#include "../exceptions/commandexception.h"

enum class RotateDirection {
    CLOCKWISE = -1,
    COUNTER_CLOCKWISE = 1
};

template<typename T>
class RotateCommand : public ICommand {
public:
    explicit RotateCommand(uint64_t objectId, RotateDirection rotateDirection = RotateDirection::COUNTER_CLOCKWISE)
                        : m_ObjectId(objectId), m_RotateDirection(rotateDirection) { }

    void execute() override {
        try {
            std::shared_ptr<IObjectsStorage> storage;
            storage = Ioc::resolve<IObjectsStorage>("Storage");
            auto object = storage->getObject(m_ObjectId);
            if(!object) {
                return;
            }

            RotatableAdapter<T> adapter(object.value());

            auto direction = adapter.getDirection();
            m_startDirection = direction;
            double angularVelocity = adapter.getAngularVelocity();
            adapter.setDirection(math::rotateVec2(direction, angularVelocity * static_cast<int>(m_RotateDirection)));
        } catch (std::exception& ex) {
            throw CommandException("No available storages found", "PrintHandler");
        }
    }

private:
    uint64_t m_ObjectId;
    RotateDirection m_RotateDirection;
    T m_startDirection;
};