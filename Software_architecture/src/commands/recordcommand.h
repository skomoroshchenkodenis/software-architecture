#pragma once

#include <vector>

#include "icommand.h"
#include "../ifaces/iuobject.h"
#include "../ioc/ioc.h"
#include "../game/iobjectsstorage.h"

class RecordCommand : public ICommand {
public:
    RecordCommand(std::vector<std::string> fields, uint64_t id) : m_Fields(std::move(fields)), m_Id(id) { }


    void execute() override {
        try {
            auto storage = Ioc::resolve<IObjectsStorage>("Storage");
            auto object = storage->getObject(m_ObjectId);
            if(!object) {
                return;
            }
            std::map<std::string, std::any> savedFields;
            for(const auto& field : m_Fields) {
                savedFields[field] = object.value()->getProperty(field);
            }
            object.value()->setProperty("CurrentState", savedFields);
        } catch (std::runtime_error& ex) {
            throw ex;
        }
    }

private:
    std::vector<std::string> m_Fields;
    uint64_t m_Id;
};