#pragma once

#include <stdexcept>
#include <string>

class CommandException : public std::runtime_error {
public:
    CommandException(std::string message, std::string handlerCommand) : m_Message(std::move(message)),
    m_Handler(std::move(handlerCommand)) ,std::runtime_error(message) { }

    const char* what() const noexcept override {
        return m_Message.c_str();
    }

    virtual std::string getHandler() const{
        return m_Handler;
    }
private:
    std::string m_Message;
    std::string m_Handler;
};