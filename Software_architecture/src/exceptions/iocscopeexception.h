#pragma once

#include <stdexcept>
#include <string>

class IocScopeException : public std::runtime_error {
public:
    IocScopeException(std::string message) : std::runtime_error(message), m_Message(std::move(message)) {}

    const char* what() const noexcept override {
        return m_Message.c_str();
    }
private:
    std::string m_Message;
};