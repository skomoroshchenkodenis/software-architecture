#pragma once

#include <stdexcept>
#include <vector>

#include "commandexception.h"

class CollisionException : public CommandException {
public:
    CollisionException(std::string message, std::string handlerCommand, std::vector<uint64_t> objectsToDelete)
            : CommandException(message, handlerCommand), m_ObjectsToDelete(std::move(objectsToDelete)) { }

    const char* what() const noexcept override {
        return "Collision happened";
    }

    std::vector<uint64_t> getObjects() {
        return m_ObjectsToDelete;
    }

private:
    std::string m_Message;
    std::vector<uint64_t> m_ObjectsToDelete;
};