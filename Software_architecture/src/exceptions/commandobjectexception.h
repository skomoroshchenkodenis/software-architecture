#pragma once

#include "commandexception.h"

class CommandObjectException : public CommandException {
public:
    CommandObjectException(std::string message, std::string handlerCommand, uint64_t id)
        : CommandException(message, handlerCommand), m_Id(id) {}


    uint64_t getId() const {
        return m_Id;
    }
private:
    uint64_t m_Id;
};