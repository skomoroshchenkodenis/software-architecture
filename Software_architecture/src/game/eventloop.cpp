#include "eventloop.h"

#include "../commands/icommand.h"
#include "../commands/exceptionhandlercommand.h"

EventLoop::EventLoop(std::shared_ptr<IQueue<ICommand>> queue) :
                    m_Queue(std::move(queue)) {
    start();
}

EventLoop::~EventLoop() {
   finish();
}

void EventLoop::submit(std::shared_ptr<ICommand> command) {
    m_Queue->push(std::move(command));
}

void EventLoop::threadRoutine() {
    while (true) {
        auto task = m_Queue->pop();
        try {
            task->execute();
        } catch (...) {
            ExceptionHandlerCommand handlerCommand(task);
            handlerCommand.execute();
        }

    }
}

void EventLoop::finish() {
   m_Thread.join();
}

void EventLoop::start() {
    m_Thread = std::thread([this](){
        threadRoutine();
    });
}
