#pragma once

#include <cstddef>
#include <memory>
#include <vector>
#include <thread>

#include "../ifaces/iqueue.h"

class ICommand;

class EventLoop {
public:
    EventLoop(std::shared_ptr<IQueue<ICommand>> queue);
    ~EventLoop();

    void submit(std::shared_ptr<ICommand> command);
    void finish();
    void start();

private:
    void threadRoutine();

private:
    std::thread m_Thread;
    std::shared_ptr<IQueue<ICommand>> m_Queue;
};