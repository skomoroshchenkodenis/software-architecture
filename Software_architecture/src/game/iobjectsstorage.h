#pragma once

#include <memory>
#include <optional>

#include "../ifaces/iuobject.h"

class IObjectsStorage {
public:
    virtual ~IObjectsStorage() = default;

    virtual std::optional<std::shared_ptr<IUObject>> getObject(uint64_t id) noexcept = 0;
    virtual uint64_t addObject(std::shared_ptr<IUObject> object) = 0;
    virtual void deleteObject(uint64_t id) = 0;
};