#pragma once

#include <utility>
#include <cmath>

template<typename T>
struct vec2_t {
    T x;
    T y;
    friend auto operator<=>(const vec2_t&, const vec2_t&) = default;

    vec2_t operator+(const vec2_t& other) {
        return {x + other.x, y + other.y};
    }
};

namespace math {

    constexpr double radians(double degrees) {
        return degrees * 0.01745329251994329576923690768489;
    }

    constexpr double degrees(double radians) {
        return radians * 57.295779513082320876798154814105;
    }

    template<typename T>
    constexpr double round(T value, std::size_t digits = 3) {
        return std::round(value * digits) / digits;
    }

    template<typename T, std::size_t digits = 3>
    constexpr double lengthVec2(const vec2_t<T>& vec){
        return round<T>(std::sqrt(vec.x * vec.x + vec.y * vec.y), digits);
    }

    template<typename T, std::size_t digits = 3>
    constexpr double scalar(const vec2_t<T>& lhs, const vec2_t<T>& rhs) {
        return round<T>(lhs.x * rhs.x + lhs.y * rhs.y, digits);
    }

    template<typename T, std::size_t digits = 3>
    constexpr vec2_t<T> rotateVec2(const vec2_t<T>& vec, double deg) {
        double angle = radians(deg);
        return {round<T>(std::cos(angle) * vec.x - std::sin(angle) * vec.y, digits),
                round<T>(std::sin(angle) * vec.x + std::cos(angle) * vec.y, digits)};
    }

    template<typename T, std::size_t digits = 5>
    constexpr double angleBetweenVec2(const vec2_t<T>& lhs, const vec2_t<T>& rhs) {
        return std::atan2(lhs.x * rhs.y - lhs.y * rhs.x, lhs.x * rhs.x + lhs.y * rhs.y);
    }
}