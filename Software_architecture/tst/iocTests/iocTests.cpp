#include <gtest/gtest.h>

#include <ioc/ioc.h>

class Foo {
public:
    virtual int getNumber() = 0;
};

class Bar : public Foo {
public:
    Bar(int number) : m_Number(number) {}

    int getNumber() override {
        return m_Number;
    }

private:
    int m_Number;
};


class TestIoc : public testing::Test {
protected:
    TestIoc() { }
    virtual ~TestIoc() {};

    virtual void SetUp() override {
        Ioc::init();

    }

    virtual void TearDown() override {
        Ioc::clear();
    }
};


TEST_F(TestIoc, testResolve) {
    Ioc::resolve<ICommand>(std::string("Ioc.Register"), std::string("Foo"),
                           lambdaConverter([](std::vector<std::any> params) -> std::shared_ptr<Foo> {
           return std::make_shared<Bar>(std::any_cast<int>(params.at(0)));
    }))->execute();
    auto bar = Ioc::resolve<Foo>("Foo", 1);
    ASSERT_EQ(1, bar->getNumber());
}

TEST_F(TestIoc, testResolveException) {
    EXPECT_THROW(Ioc::resolve<Foo>("Foo", 1), std::invalid_argument);
}

TEST_F(TestIoc, testNoScopeException) {
    Ioc::clear();
    EXPECT_THROW(Ioc::resolve<Foo>("Foo", 1), IocScopeException);
}