#pragma once
#include <gtest/gtest.h>
#include <utility>

using ::testing::Return;
using ::testing::_;
using ::testing::An;
using ::testing::TypedEq;
using ::testing::ByMove;


template<typename T, typename U>
class AnyMatcherImpl {
public:
    using is_gtest_matcher = void;

    AnyMatcherImpl(U u) : m_ExpectedValue(u) {

    }

    bool MatchAndExplain(std::any n, std::ostream*) const {
        return std::any_cast<T>(n) == m_ExpectedValue;
    }

    void DescribeTo(std::ostream* os) const {
        *os << "any ok";
    }

    void DescribeNegationTo(std::ostream* os) const {
        *os << "any wrong";
    }

private:
    U m_ExpectedValue;
};

template<typename T, typename U>
::testing::Matcher<const std::any&> AnyMatcher(U&& u) {
    return AnyMatcherImpl<T, U>(std::forward<decltype(u)>(u));
}