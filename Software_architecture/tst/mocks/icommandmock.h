#pragma once

#include <commands/icommand.h>
#include <gmock/gmock.h>

class MockICommand : public ICommand {
public:
    MOCK_METHOD(void, execute, (), (override));
};
