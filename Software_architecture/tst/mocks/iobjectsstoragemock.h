#pragma once

#include <game/iobjectsstorage.h>
#include <gmock/gmock.h>

class MockIObjectsStorage : public IObjectsStorage {
public:
    MOCK_METHOD(std::optional<std::shared_ptr<IUObject>>, getObject, (uint64_t), (override, noexcept));
    MOCK_METHOD(uint64_t, addObject, (std::shared_ptr<IUObject>), (override));
    MOCK_METHOD(void, deleteObject, (uint64_t), (override));
};
