#pragma once

#include <ifaces/iqueue.h>
#include <gmock/gmock.h>

class MockIQueue : public IQueue<ICommand> {
public:
    MOCK_METHOD(void, push, (std::shared_ptr<ICommand> command), (override));
    MOCK_METHOD(std::shared_ptr<ICommand>, pop, (), (override));
};