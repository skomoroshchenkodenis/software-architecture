#pragma once

#include <ifaces/iuobject.h>
#include <gmock/gmock.h>

class MockUObject : public IUObject {
public:
    MOCK_METHOD(std::any, getProperty, (const std::string& name), (override));
    MOCK_METHOD(void, setProperty, (const std::string& name, const std::any& value), (override));
};
