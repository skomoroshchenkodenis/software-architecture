#include <gtest/gtest.h>

#include <utils.h>

TEST(Vec2Tests, plusOperatorTest) {
    vec2_t<double> vec1 {1.0, 3.0};
    vec2_t<double> vec2 {-1.0, 5.0};

    vec2_t<double> result {0, 8.0};
    EXPECT_EQ(result, vec1 + vec2);
}