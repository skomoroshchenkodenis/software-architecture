#include <gtest/gtest.h>

#include <utils.h>

TEST(MathTest, radiansTest) {
    double rad1 = math::radians(30);
    double rad2 = math::radians(45);
    double rad3 = math::radians(60);

    EXPECT_DOUBLE_EQ(std::round(rad1 * 100) / 100, 0.52);
    EXPECT_DOUBLE_EQ(std::round(rad2 * 100) / 100, 0.79);
    EXPECT_DOUBLE_EQ(std::round(rad3 * 100) / 100, 1.05);
}

TEST(MathTest, degreeTest) {
    double deg1 = math::degrees(0.52);
    double deg2 = math::degrees(0.79);
    double deg3 = math::degrees(1.05);

    EXPECT_DOUBLE_EQ(std::round(deg1), 30.0);
    EXPECT_DOUBLE_EQ(std::round(deg2), 45.0);
    EXPECT_DOUBLE_EQ(std::round(deg3), 60.0);
}

TEST(MathTest, vec2LengthTest) {
    vec2_t<double> vec1 {3.0, 4.0};
    double length1 = math::lengthVec2(vec1);
    vec2_t<double> vec2 {-3.0, -4.0};
    double length2 = math::lengthVec2(vec2);

    EXPECT_DOUBLE_EQ(length1, 5.0);
    EXPECT_DOUBLE_EQ(length2, 5.0);
}

TEST(MathTest, scalarVec2Test) {
    vec2_t<double> vec1 {3.0, 4.0};
    vec2_t<double> vec2 {4.0, 3.0};

    double scalar = math::scalar(vec1, vec2);
    EXPECT_DOUBLE_EQ(scalar, 24.0);
}

TEST(MathTest, rotateVec2Test) {
    vec2_t<double> vec1{1.0, 0.0};

    vec2_t<double> result {0.0, -1.0};
    vec2_t<double> ans = math::rotateVec2(vec1, -90.0);
    EXPECT_EQ(result, ans);
}

TEST(MathTest, angleBetweenVec2) {
    vec2_t<double> vec1 {1.0, 0.0};
    vec2_t<double> vec2{0.0, 1.0};
    EXPECT_DOUBLE_EQ(math::degrees(math::angleBetweenVec2(vec1, vec2)), 90.0);
    EXPECT_DOUBLE_EQ(math::degrees(math::angleBetweenVec2(vec2, vec1)), -90.0);
}