#include <gtest/gtest.h>

#include <commands/exceptionhandlers/collisionhandlercommand.h>
#include <ioc/ioc.h>

#include "../mocks/iobjectsstoragemock.h"
#include "../mocks/iuobjectmock.h"

using ::testing::Return;
using ::testing::_;
using ::testing::An;
using ::testing::TypedEq;
using ::testing::ByMove;

class TestCollisionHandler : public testing::Test {
protected:
    TestCollisionHandler() { }
    virtual ~TestCollisionHandler() {};

    void SetUp() override {
        Ioc::init();
        auto storage = std::make_shared<MockIObjectsStorage>();
        Ioc::resolve<ICommand>(std::string("Ioc.Register"), std::string("Storage"),
                               lambdaConverter([storage](std::vector<std::any> params) -> std::shared_ptr<IObjectsStorage> {
                                   return storage;
                               }))->execute();
    }

    void TearDown() override {
        Ioc::clear();
    }
};


TEST_F(TestCollisionHandler, executeTest) {
    CollisionException ex("CollisionHappened", "CollisionHandler", {1});
    std::runtime_error& error = ex;

    CollisionHandlerCommand command(error);

    auto mockStorage = std::static_pointer_cast<MockIObjectsStorage>(Ioc::resolve<IObjectsStorage>("Storage"));

    EXPECT_CALL(*mockStorage, deleteObject(1)).Times(1);

    command.execute();
}