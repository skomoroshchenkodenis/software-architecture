#include <gtest/gtest.h>

#include <ifaces/iuobject.h>
#include <commands/shootcommand.h>

#include "../mocks/iuobjectmock.h"
#include "../matchers/anymatcher.h"
#include "../mocks/iobjectsstoragemock.h"
#include "../mocks/iqueuemock.h"

using ::testing::Return;
using ::testing::_;
using ::testing::An;
using ::testing::TypedEq;
using ::testing::ByMove;

class TestShootCommand : public testing::Test {
protected:
    TestShootCommand() { }
    virtual ~TestShootCommand() {};

    void SetUp() override {
        Ioc::init();
        auto storage = std::make_shared<MockIObjectsStorage>();
        Ioc::resolve<ICommand>(std::string("Ioc.Register"), std::string("Storage"),
                               lambdaConverter([storage](std::vector<std::any> params) -> std::shared_ptr<IObjectsStorage> {
                                   return storage;
                               }))->execute();

        auto mock = std::make_shared<MockIQueue>();
        Ioc::resolve<ICommand>(std::string("Ioc.Register"), std::string("Queue"),
                               lambdaConverter([mock](std::vector<std::any> params) -> std::shared_ptr<IQueue<ICommand>> {
                                   return mock;
                               }))->execute();

        auto mockBullet = std::make_shared<MockUObject>();
        Ioc::resolve<ICommand>(std::string("Ioc.Register"), std::string("Bullet"),
                               lambdaConverter([mockBullet](std::vector<std::any> params) -> std::shared_ptr<IUObject> {
                                   return mockBullet;
                               }))->execute();
        auto mockObj = std::make_shared<MockUObject>();
        Ioc::resolve<ICommand>(std::string("Ioc.Register"), std::string("MockUObject"),
                               lambdaConverter([mockObj](std::vector<std::any> params) -> std::shared_ptr<IUObject> {
                                   return mockObj;
                               }))->execute();
    }

    void TearDown() override {
        Ioc::clear();
    }
};


TEST_F(TestShootCommand, shootTest) {
    auto mock = std::static_pointer_cast<MockUObject>(Ioc::resolve<IUObject>("MockUObject"));
    auto mockStorage = std::static_pointer_cast<MockIObjectsStorage>(Ioc::resolve<IObjectsStorage>("Storage"));
    auto mockBullet = std::static_pointer_cast<MockUObject>(Ioc::resolve<IUObject>("Bullet"));
    auto mockIQueue = std::static_pointer_cast<MockIQueue>(Ioc::resolve<IQueue<ICommand>>("Queue"));

    EXPECT_CALL(*mockStorage, addObject(_)).Times(1).WillOnce(Return(1));

    uint64_t mockId = mockStorage->addObject(mock);
    ICommand *shootCommand = new ShootCommand<vec2_t<double>>(mockId);

    EXPECT_CALL(*mockStorage, getObject(1)).Times(1).WillOnce(Return(mock));
    EXPECT_CALL(*mock, getProperty("projectile")).Times(1).WillOnce(Return(std::any(std::string("Bullet"))));
    EXPECT_CALL(*mockStorage, addObject(_)).Times(1).WillOnce(Return(2));
    EXPECT_CALL(*mockIQueue, push(_)).Times(1);

    shootCommand->execute();

    delete shootCommand;
}