#include <gtest/gtest.h>

#include <ifaces/iuobject.h>
#include <commands/movecommand.h>

#include "../mocks/iuobjectmock.h"
#include "../mocks/iobjectsstoragemock.h"
#include "../matchers/anymatcher.h"

using ::testing::Return;
using ::testing::_;
using ::testing::An;
using ::testing::TypedEq;
using ::testing::ByMove;

class TestMovable : public testing::Test {
protected:
    TestMovable() { }
    virtual ~TestMovable() {};

    void SetUp() override {
        Ioc::init();
        auto storage = std::make_shared<MockIObjectsStorage>();
        Ioc::resolve<ICommand>(std::string("Ioc.Register"), std::string("Storage"),
                               lambdaConverter([storage](std::vector<std::any> params) -> std::shared_ptr<IObjectsStorage> {
                                   return storage;
                               }))->execute();

        auto mock = std::make_shared<MockUObject>();
        Ioc::resolve<ICommand>(std::string("Ioc.Register"), std::string("MockUObject"),
                               lambdaConverter([mock](std::vector<std::any> params) -> std::shared_ptr<IUObject> {
                                   return mock;
                               }))->execute();
    }

    void TearDown() override {
        Ioc::clear();
    }
};


TEST_F(TestMovable, moveStraight1){
    auto mock = std::static_pointer_cast<MockUObject>(Ioc::resolve<IUObject>("MockUObject"));
    auto mockStorage = std::static_pointer_cast<MockIObjectsStorage>(Ioc::resolve<IObjectsStorage>("Storage"));

    EXPECT_CALL(*mockStorage, addObject(_)).Times(1).WillOnce(Return(1));

    uint64_t mockId = mockStorage->addObject(mock);
    std::shared_ptr<ICommand> moveCommand = std::make_shared<MoveCommand<vec2_t<double>>>(mockId);

    EXPECT_CALL(*mockStorage, getObject(1)).Times(1).WillOnce(Return(mock));
    EXPECT_CALL(*mock, getProperty("position")).Times(1).WillOnce(Return(std::any(vec2_t<double>{1.0, 5.0})));
    EXPECT_CALL(*mock, getProperty("velocity")).Times(1).WillOnce(Return(std::any(vec2_t<double>{5.0, -3.0})));
    EXPECT_CALL(*mock, setProperty(TypedEq<const std::string&>("position"),
                                   AnyMatcher<vec2_t<double>>(vec2_t<double>{6.0, 2.0})));

    moveCommand->execute();
}