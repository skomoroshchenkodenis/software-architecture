#include <gtest/gtest.h>

#include <ifaces/iuobject.h>

#include "../mocks/iuobjectmock.h"
#include "../matchers/anymatcher.h"
#include "adapters/rotatableadapter.h"

using ::testing::Return;
using ::testing::_;
using ::testing::An;
using ::testing::TypedEq;
using ::testing::ByMove;


TEST(TestRotatableAdapter, getSetTest) {
    std::shared_ptr<MockUObject> mock = std::make_shared<MockUObject>();
    RotatableAdapter adapter(mock);

    EXPECT_CALL(*mock, setProperty(TypedEq<const std::string&>("direction"),
                                   AnyMatcher<vec2_t<double>>(vec2_t<double>{6.0, 2.0})));
    EXPECT_CALL(*mock, getProperty("direction")).Times(1).WillOnce(Return(std::any(vec2_t<double>{6.0, 2.0})));

    adapter.setDirection(vec2_t<double>{6.0, 2.0});
    adapter.getDirection();
}