#include <gtest/gtest.h>

#include <adapters/rotatableadapter.h>
#include <commands/endrotationcommand.h>
#include <commands/rotatecommand.h>

#include "../mocks/iuobjectmock.h"
#include "../mocks/iobjectsstoragemock.h"

using ::testing::_;
using ::testing::Return;

class TestEndRotation : public testing::Test {
protected:
    TestEndRotation() { }
    virtual ~TestEndRotation() {};

    void SetUp() override {
        Ioc::init();
        auto storage = std::make_shared<MockIObjectsStorage>();
        Ioc::resolve<ICommand>(std::string("Ioc.Register"), std::string("Storage"),
                               lambdaConverter([storage](std::vector<std::any> params) -> std::shared_ptr<IObjectsStorage> {
                                   return storage;
                               }))->execute();

        auto mock = std::make_shared<MockUObject>();
        Ioc::resolve<ICommand>(std::string("Ioc.Register"), std::string("MockUObject"),
                               lambdaConverter([mock](std::vector<std::any> params) -> std::shared_ptr<IUObject> {
                                   return mock;
                               }))->execute();
    }

    void TearDown() override {
        Ioc::clear();
    }
};

TEST_F(TestEndRotation, replaceRotateCommandTest) {
    auto mock = std::static_pointer_cast<MockUObject>(Ioc::resolve<IUObject>("MockUObject"));
    auto mockStorage = std::static_pointer_cast<MockIObjectsStorage>(Ioc::resolve<IObjectsStorage>("Storage"));

    EXPECT_CALL(*mockStorage, addObject(_)).Times(1).WillOnce(Return(1));

    uint64_t mockId = mockStorage->addObject(mock);
    std::shared_ptr<EndRotationCommand> endMoveCommand = std::make_shared<EndRotationCommand>(mockId);

    EXPECT_CALL(*mockStorage, getObject(1)).Times(1).WillOnce(Return(mock));
    EXPECT_CALL(*mock, getProperty("rotateCommand")).Times(1).
            WillOnce(Return(std::any(std::shared_ptr<ICommand>(new RotateCommand<vec2_t<double>>(mockId,
                    RotateDirection::COUNTER_CLOCKWISE)))));
    EXPECT_CALL(*mock, setProperty("rotateCommand", _));

    endMoveCommand->execute();
}