#include <gtest/gtest.h>

#include <commands/exceptionhandlers/collisionhandlercommand.h>
#include <commands/exceptionhandlers/undocommandexceptionhandler.h>
#include <ioc/ioc.h>
#include <exceptions/commandobjectexception.h>

#include "../mocks/iobjectsstoragemock.h"
#include "../mocks/iuobjectmock.h"
#include "../matchers/anymatcher.h"

using ::testing::Return;
using ::testing::_;
using ::testing::An;
using ::testing::TypedEq;
using ::testing::ByMove;


class TestUndo : public testing::Test {
protected:
    TestUndo() { }
    virtual ~TestUndo() {};

    void SetUp() override {
        Ioc::init();
        auto storage = std::make_shared<MockIObjectsStorage>();
        Ioc::resolve<ICommand>(std::string("Ioc.Register"), std::string("Storage"),
                               lambdaConverter([storage](std::vector<std::any> params) -> std::shared_ptr<IObjectsStorage> {
                                   return storage;
                               }))->execute();

        auto mock = std::make_shared<MockUObject>();
        Ioc::resolve<ICommand>(std::string("Ioc.Register"), std::string("MockUObject"),
                               lambdaConverter([mock](std::vector<std::any> params) -> std::shared_ptr<IUObject> {
                                   return mock;
                               }))->execute();
    }

    void TearDown() override {
        Ioc::clear();
    }
};

TEST_F(TestUndo, testUndo) {
    auto mock = std::static_pointer_cast<MockUObject>(Ioc::resolve<IUObject>("MockUObject"));
    auto mockStorage = std::static_pointer_cast<MockIObjectsStorage>(Ioc::resolve<IObjectsStorage>("Storage"));

    CommandObjectException ex("message", "UndoCommand", 1);
    std::runtime_error& error = ex;


    UndoExceptionCommandHandler handler(ex);

    std::map<std::string, std::any> fields = {{"id", std::any(2)}};

    EXPECT_CALL(*mockStorage, getObject(1)).Times(1).WillOnce(Return(mock));
    EXPECT_CALL(*mock, getProperty("CurrentState")).Times(1).WillOnce(Return(std::any(fields)));
    EXPECT_CALL(*mock, setProperty(TypedEq<const std::string&>("id"),
                                   AnyMatcher<int>(2)));

    handler.execute();
}