#include <gtest/gtest.h>

#include <commands/repeatcommand.h>

#include "../mocks/iqueuemock.h"
#include "../mocks/icommandmock.h"

using ::testing::_;

TEST(TestRepeatCommand, pushToQueueTest) {
    std::shared_ptr<MockIQueue> queue = std::make_shared<MockIQueue>();
    std::shared_ptr<MockICommand> command = std::make_shared<MockICommand>();
    RepeatCommand repeatCommand(command, *queue);

    EXPECT_CALL(*queue, push(_));

    repeatCommand.execute();
}
