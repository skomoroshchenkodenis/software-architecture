#include <gtest/gtest.h>

#include <commands/macrocommand.h>

#include "../mocks/icommandmock.h"


using ::testing::Return;
using ::testing::_;
using ::testing::An;
using ::testing::TypedEq;
using ::testing::ByMove;


TEST(TestMacroCommand, executeCommandsTest) {

    std::shared_ptr<MockICommand> mock1 = std::make_shared<MockICommand>();
    std::shared_ptr<MockICommand> mock2 = std::make_shared<MockICommand>();
    std::shared_ptr<MockICommand> mock3 = std::make_shared<MockICommand>();

    MacroCommand *macro = new MacroCommand({mock1, mock2});
    macro->addCommand(std::shared_ptr<ICommand>(mock3));

    EXPECT_CALL(*mock1, execute());
    EXPECT_CALL(*mock2, execute());
    EXPECT_CALL(*mock3, execute());

    macro->execute();
    delete macro;
}