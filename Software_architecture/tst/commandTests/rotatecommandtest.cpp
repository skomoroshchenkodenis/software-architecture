#include <gtest/gtest.h>

#include <ifaces/iuobject.h>
#include <commands/rotatecommand.h>

#include "../mocks/iuobjectmock.h"
#include "../matchers/anymatcher.h"
#include "../mocks/iobjectsstoragemock.h"

using ::testing::Return;
using ::testing::_;
using ::testing::An;
using ::testing::TypedEq;
using ::testing::ByMove;

class TestRotateCommand : public testing::Test {
protected:
    TestRotateCommand() { }
    virtual ~TestRotateCommand() {};

    void SetUp() override {
        Ioc::init();
        auto storage = std::make_shared<MockIObjectsStorage>();
        Ioc::resolve<ICommand>(std::string("Ioc.Register"), std::string("Storage"),
                               lambdaConverter([storage](std::vector<std::any> params) -> std::shared_ptr<IObjectsStorage> {
                                   return storage;
                               }))->execute();

        auto mock = std::make_shared<MockUObject>();
        Ioc::resolve<ICommand>(std::string("Ioc.Register"), std::string("MockUObject"),
                               lambdaConverter([mock](std::vector<std::any> params) -> std::shared_ptr<IUObject> {
                                   return mock;
                               }))->execute();
    }

    void TearDown() override {
        Ioc::clear();
    }
};

TEST_F(TestRotateCommand, rotateClockwiseTest) {
    auto mock = std::static_pointer_cast<MockUObject>(Ioc::resolve<IUObject>("MockUObject"));
    auto mockStorage = std::static_pointer_cast<MockIObjectsStorage>(Ioc::resolve<IObjectsStorage>("Storage"));

    EXPECT_CALL(*mockStorage, addObject(_)).Times(1).WillOnce(Return(1));

    uint64_t mockId = mockStorage->addObject(mock);
    ICommand *rotateCommand = new RotateCommand<vec2_t<double>>(mockId, RotateDirection::CLOCKWISE);

    EXPECT_CALL(*mockStorage, getObject(1)).Times(1).WillOnce(Return(mock));
    EXPECT_CALL(*mock, getProperty("direction")).Times(1).WillOnce(Return(std::any(vec2_t<double>{0.0, 1.0})));
    EXPECT_CALL(*mock, getProperty("angularVelocity")).Times(1).WillOnce(Return(std::any(90.0)));
    EXPECT_CALL(*mock, setProperty(TypedEq<const std::string&>("direction"),
                                   AnyMatcher<vec2_t<double>>(vec2_t<double>{1.0, 0.0})));

    rotateCommand->execute();
    delete rotateCommand;
}

TEST_F(TestRotateCommand, rotateCounterClockwiseTest) {
    auto mock = std::static_pointer_cast<MockUObject>(Ioc::resolve<IUObject>("MockUObject"));
    auto mockStorage = std::static_pointer_cast<MockIObjectsStorage>(Ioc::resolve<IObjectsStorage>("Storage"));

    EXPECT_CALL(*mockStorage, addObject(_)).Times(1).WillOnce(Return(1));

    uint64_t mockId = mockStorage->addObject(mock);
    ICommand *rotateCommand = new RotateCommand<vec2_t<double>>(mockId, RotateDirection::COUNTER_CLOCKWISE);

    EXPECT_CALL(*mockStorage, getObject(1)).Times(1).WillOnce(Return(mock));
    EXPECT_CALL(*mock, getProperty("direction")).Times(1).WillOnce(Return(std::any(vec2_t<double>{0.0, 1.0})));
    EXPECT_CALL(*mock, getProperty("angularVelocity")).Times(1).WillOnce(Return(std::any(90.0)));
    EXPECT_CALL(*mock, setProperty(TypedEq<const std::string&>("direction"),
                                   AnyMatcher<vec2_t<double>>(vec2_t<double>{-1.0, 0.0})));

    rotateCommand->execute();
    delete rotateCommand;
}
